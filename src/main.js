import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";
import '../node_modules/element-plus/lib/theme-chalk/index.css'
import ElementPlus from 'element-plus'
import axios from "axios"
import 'dayjs/locale/zh-cn'
import locale from 'element-plus/lib/locale/lang/zh-cn'


const app = createApp(App)
app.use(router)
app.use(ElementPlus, { locale })

app.config.globalProperties.$axios = axios
app.config.productionTip = false

app.mount('#app')