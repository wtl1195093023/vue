import {createRouter, createWebHashHistory} from 'vue-router'
import userService from "../utils/userService";
import {ElMessage} from "element-plus";



const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            name: '主页',
            component:  import('../pages/Home.vue')
        },
        {
            path: '/user',
            name: '用户管理',
            component:  import('../pages/UserManage.vue')
        },
        {
            path:'/Researcher',
            name:'Researcher',
            component:  import('../components/Researcher.vue')
        },
        {
            path:'/EstablishProj',
            name:'Estab',
            component:  import('../components/EstablishProj.vue')
        },
        {
            path:'/PermittedProj',
            name:'PermittedProj',
            component:  import('../components/PermittedProj.vue')
        },
        {
            path:'/MyProj',
            name:'MyProj',
            component:  import('../components/MyProj.vue')
        },
        {
            path:'/MyApply',
            name:'MyApply',
            component:  import('../components/MyApply.vue')
        },
        {
            path: '/Leader',
            name: 'Leader管理',
            component:  import('../pages/leader-manage/leader.vue')
        },
        {
            path: '/Export',
            name: '报表管理',
            component:  import('../pages/excel/index.vue')
        },
        {
            path: '/MemberExport',
            name: '团队成员报表',
            component:  import('../pages/excel/MemberExport.vue')
        },
        {
            path:'/projectDrawBar',
            name:'项目块状图',
            component:import('../components/projectDrawBar.vue')
        },
        {
            path:'/projectDrawLine',
            name:'项目折线图',
            component:import('../components/projectDrawLine.vue')
        },
        {
            path:'/projectDrawPie',
            name:'项目饼图',
            component:import('../components/projectDrawPie.vue')
        },
        {
            path:'/projectJADrawBar',
            name:'项目申请块状图',
            component:import('../components/projectJADrawBar.vue')
        },
        {
            path:'/projectJADrawLine',
            name:'项目申请折线图',
            component:import('../components/projectJADrawLine.vue')
        },
        {
            path:'/projectJADrawPie',
            name:'项目申请饼图',
            component:import('../components/projectJADrawPie.vue')
        },
        {
            path:'/logDrawBar',
            name:'日志块状图',
            component:import('../components/logDrawBar.vue')
        },
        {
            path:'/logDrawLine',
            name:'日志折线图',
            component:import('../components/logDrawLine.vue')
        },
        {
            path:'/logDrawPie',
            name:'日志饼图',
            component:import('../components/logDrawPie.vue')
        },
        {
            path:'/ProjectManage',
            name:'项目审核',
            component:  import('../pages/leader-manage/ProjectManage.vue')
        }
    ]
})


router.beforeEach((to, from, next) => {
    if (to.path !== '/' && !sessionStorage.getItem("c_user")){  // 未登录用户只能访问主页
        next({path:'/'})
        ElMessage.warning({type:"warning" ,message:"请先登录!", zIndex:-1});
    }else {
        next()
    }
})

export default router
